package model;

import java.util.List;
// clasa de punere a clientilor in cozi dupa timpul cel mai scurt
public class ConcreteStrategyTime implements Strategy {
    @Override
    // metoda de adaugare
    public void addTask(List<Server> servers, Task task)  {
        int minTime=-1; // timp minim
        int index =0,i=-1; // indexul curent
        for(Server s : servers) { // pt toate serverele, setez timpul 0 si il calzulez pe rand
            int curTime = 0;
            i++;
            for(Task t : s.getTasks()) // calzulez timpul de procesare pe coada
           {
               curTime += t.getProcessingTime();
           }
           if(minTime==-1 || curTime<minTime) // daca timpul e mai scurt decat minimul
           {
               index=i; // salvez pozitia buna
               minTime = curTime; // si timpul
           }
        }
        servers.get(index).addTask(task); // adaug pe coada index clientul
    }
}
