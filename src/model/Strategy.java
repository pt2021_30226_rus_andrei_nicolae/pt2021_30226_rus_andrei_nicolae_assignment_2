package model;

import java.util.List;
// interfata pentru adugarea de client in coada
public interface Strategy {
    public void addTask(List<Server> servers, Task t) ;
}
