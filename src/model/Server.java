package model;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
// clasa Server e teoretic o Coada thread
public class Server implements Runnable{

    private BlockingQueue<Task> tasks; // coada de clienti
    private boolean time = true; // var booleana de timp
    public Server(){ // init
        tasks = new LinkedBlockingQueue<>();
    }
    public void addTask(Task task) {
        tasks.add(task); // adaugare de clienti
    }
    @Override
    public void run()  { // metoda suprasrisa de implementare supra timp
        while(time) { // cat timp exista timp
                while (!tasks.isEmpty()) { // cat timp coada nu e goala
                    try{
                        BlockingQueue<Task> copy = new LinkedBlockingQueue<>(); // iau o copie

                        for(Task t : tasks)
                            copy.add(t); // le copiez

                        Task task = copy.remove(); //iau primul element din lista
                        Thread.sleep(task.getProcessingTime()*1000); // blochez coada cu un timp=processingTime
                        tasks.remove(); // scot din coada cand s-a prelucrat clientul

                    } catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }

        }
    }
    public BlockingQueue<Task> getTasks()
    {
        return tasks;
    } // getter clienti
    public void stop() // metoda de oprire a serbverului
    {
        this.time = false;
    }
}
