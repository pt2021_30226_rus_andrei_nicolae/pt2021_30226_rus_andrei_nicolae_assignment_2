package model;

import java.util.List;
// clasa de punere a clientilor in cozi dupa cea mai scurta
public class ConcreteStategyQueue implements Strategy {
    @Override
    public void addTask(List<Server> servers, Task t)
    {
        for(Server s : servers) // iau toate cozile
        {
            if(s.getTasks().size()==0) { // daca gasesc coada goala
                s.addTask(t); // pun clientul acolo si ma opresc
                break;
            }
        }
    }
}
