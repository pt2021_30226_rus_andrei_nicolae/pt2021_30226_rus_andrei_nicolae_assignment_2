package model;

public class Task {
    private  int arrivalTime; // timp de start
    private int id; // id
    private int processingTime; // timpul de procesare
    public Task(int arrivalTime,int processingTime,int id) // initializare
    {
        this.arrivalTime = arrivalTime;
        this.processingTime = processingTime;
        this.id = id;
    }
    public int getId()
    {
        return id;
    } // getter id
    public int getFinishTime()
    {
        return arrivalTime + processingTime;
    } // getter finishTime

    public int getArrivalTime() {
        return arrivalTime;
    } // getter arrivalTime

    public int getProcessingTime() {
        return processingTime;
    } // getter processingTime
    public void decrementPorcessingTime(){
        processingTime--;
    } // decrementare timp de procesare
}
