package model;
// clasa de calculare a timpului cu cel mai mare trafic
public class PeakTime {
    private int time; // timpul
    private double process; // timpul total de procesare pe respectivul timp
    public PeakTime() // initializare
    {
        time=0;
        process=-1;
    }
    public void setTime(int time)
    {
        this.time=time;
    } // setter timp
    public void setProcess(double process)
    {
        this.process=process;
    } // setter process
    public int getTime()
    {
        return time;
    } // getter time
    public double getProcess()
    {
        return process;
    } // getter porces
}
