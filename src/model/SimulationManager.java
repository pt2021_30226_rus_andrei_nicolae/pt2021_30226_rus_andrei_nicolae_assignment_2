package model;
import view.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static java.lang.Integer.parseInt;
// clasa ce reprezinta simularea propriu zisa
public class SimulationManager implements Runnable{
    private ViewInput viewInput; // interfata de intrare
    public int timeLimit=50; // timpul limita
    public int maxProcessingTime=10; // timpul min si max de servire
    public int minProcessingTime=2;
    public int maxArrivalTime=30; // timpul max si min de "ajungere"
    public int minArrivalTime=1;
    public int numberOfServers=4; // nr de servere
    public int numberOfClients=25; // nr de clienti
    public FileWriter writer; // fisier de scriere
    public SelectionPolicy selectionPolicy; // selectia
    public ViewOutput viewOutput; // interfata grafica de iesire


    private Scheduler scheduler; // planificarea
    // private SimulationFrame frame;
    private List<Task> generatedTasks; // clientii generati

    public SimulationManager(ViewInput viewInput) throws IOException {
        writer = new FileWriter("fisier.txt");
        // initializare interfete grafice si citire componente de la intrare.
        this.viewInput = viewInput;
        viewOutput = new ViewOutput();
        timeLimit = parseInt(viewInput.getSimulationInterval().getText()); // etc-> citire din input
        maxProcessingTime = parseInt(viewInput.getMaxService().getText());
        minProcessingTime = parseInt(viewInput.getMinService().getText());
        maxArrivalTime = parseInt(viewInput.getMaxArrival().getText());
        minArrivalTime = parseInt(viewInput.getMinArrival().getText());
        numberOfServers = parseInt(viewInput.getQueues().getText());
        numberOfClients = parseInt(viewInput.getNrClients().getText());
        // initializare lista de clienti
        generatedTasks = new ArrayList<>();
        scheduler = new Scheduler(numberOfServers,numberOfClients); // init planificare
        this.generateNRandomTasks(); // se genereaza clienti conform metodei
    }
    private void generateNRandomTasks() // metoda de generare random de clienti
    {
        Random rand = new Random(); // obiect random
        for(int i=0 ; i<numberOfClients ; i++) // mergpe toti clientii
        {   // generez in intervalul citit un timp random de arrive, process si idul conform indicelui
            Task t= new Task(rand.nextInt(maxArrivalTime-minArrivalTime)+minArrivalTime,rand.nextInt(maxProcessingTime-minProcessingTime)+minProcessingTime,i+1) ;
            generatedTasks.add(t);
            // adaug in lista de clienti
        }
        Collections.sort(generatedTasks, new Comparator<Task>() { // sortare ascendenta dupa timpul de sosire
            @Override
            public int compare(Task t1, Task t2) {
                return t1.getArrivalTime()-t2.getArrivalTime();
            }
        });
    }
    @Override
    public void run() { // metoda principala thread
        int currentTime = 0; // timpul curent
        double average=0,averageService=averageServiceTime(); // timp asteptare mediu si de procesare
        PeakTime peakTime = new PeakTime(); // timpul curent si timpul de procesare
        scheduler.changeStrategy(selectionPolicy); // schimb strategia dupa lista goala
        while(currentTime <= timeLimit && (checkClients() || checkQueues())) // cat timp nu am depasit limita si mai am clienti
        {
            for(int i=0;i<generatedTasks.size();i++) { // iau toti clientii
                if (generatedTasks.get(i).getArrivalTime() <= currentTime) {   // daca timpul de sosire e <= cu timpul curent
                    doStrategy(i); // pun in coada conform strategiei
                    i--;
                }
            }
            this.peakTime(currentTime,peakTime); // calzulez timpul de peak
            average += averageTime(); // adaug si timpul mediu pe timpul curent al cozilor
            inFile("Time: "+currentTime+"\n"); // pun in fisier timpul curent
            viewOutput.setText("<html>Time: "+currentTime+"<br>"+print()); // pun pe interfata grafica de iesire, timpul curent si starea cozilor cu clientii
            inFile("\n");
            currentTime++; //  cresc timpul curent
            try{
                Thread.sleep(1000); // fac pauza de 1 secunda
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } // pun pe vedere la final, timpii medii si timpul aglomerat
        viewOutput.setText("<html>Average waiting time: " + (double)((int)(average/timeLimit*100)/100.0) +"<br><html>Average service time: "+ (double)((int)(averageService*100)/100.0)+"<br>"+"<html>Peak Time: "+ peakTime.getTime()+"<br>");
        closeFile();//inchid fisierul
        scheduler.stopServers(); // opresc cozile
    }
    private String print()  { // metoda de afisare
        String out =""; // init
        out += "<html>Waiting clients:<br> ";
        inFile("Waiting clients:");
        for(Task client: generatedTasks) // afisez clientii ce asteapta pe ecran si in fisier
        {
            out += "("+client.getId()+","+client.getArrivalTime()+","+client.getProcessingTime()+"),";
            inFile("("+client.getId()+","+client.getArrivalTime()+","+client.getProcessingTime()+"),");
        }
        inFile("\n");
        out += "<html><br>";
        int i=0;
        for(Server s : scheduler.getServers()) // afisez continutul din fiecare coada
        {
            i++;
            out += "Coada "+i+": ";
            inFile("Coada "+i+": ");
            for(Task t : s.getTasks()) // pe coada, afisez clientii
            {
                out += "("+t.getId()+","+t.getArrivalTime()+","+t.getProcessingTime()+"),"; // construiesc pe out
                inFile("("+t.getId()+","+t.getArrivalTime()+","+t.getProcessingTime()+"),");
            }
            for(Task t : s.getTasks()) // primul element din fiecare va fi decrementat cu 1
            {
               t.decrementPorcessingTime();
               break;
            }
            inFile("\n");
            out += "<html><br>";
        }
        return out; // returnez ultimul element
    }
    private void inFile(String s)  { // metoda de punere in fisier
        try {
            writer.write(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void closeFile() // metoda de inchidere fisier
    {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private double averageTime() // calcularea  timpului mediu de asteptare
    {
        double average=0;
        for(Server server : scheduler.getServers()) // iau din fiecare coada
        {
            for (Task client : server.getTasks()) // adun pt fiecare client  timpul de procesare
            {
                average += client.getProcessingTime();
            }
        }
        return average / scheduler.getServers().size(); // impart pe nr de servere
    }
    private double averageServiceTime() // metoda  de calculare a serviciului mediu pe coada
    {
        double service=0;
        for(Task task : generatedTasks) // la fel ca la AverageWait
        {
            service += task.getProcessingTime();
        }
        return service/generatedTasks.size(); // impart la nr de cozi
    }
    private boolean checkClients() //  verifica daca lista de clienti generata e goala
    {
       if(generatedTasks.size() == 0)
           return false;
       else return true;
    }
    private boolean checkQueues() // verifica daca mai exista clienti in cozi
    {
        for(Server server : scheduler.getServers())
        {
            for (Task client : server.getTasks())
            {
                return true;
            }
        }
        return false;
    }
    private void doStrategy(int i) // alege strategia pe punere in cozi
    {
        if(!scheduler.serversFull()) { // daca nu coada e full
            selectionPolicy = SelectionPolicy.SHORTEST_QUEUE; //strategie coada
        }
        else{
            selectionPolicy = SelectionPolicy.SHORTEST_TIME; // daca nu, strategie timp
        }
        scheduler.changeStrategy(selectionPolicy); // schimb strategia conform a ceea ce trebuie
        scheduler.dispatchTask(generatedTasks.get(i)); // pun in coada conform strategiei
        generatedTasks.remove(generatedTasks.get(i)); // il sterg din lista de clienti generata
    }
    private void peakTime(int time,PeakTime peakTime) // calculeaza timpul cel mai aglormerat
    {
        double average = averageTime(); // in functie de timpul mediu, iau timpul ce are timpii de procesare maximi
        if(peakTime.getProcess() < average)
        {
            peakTime.setTime(time);
            peakTime.setProcess(average);
        }
    }
}
