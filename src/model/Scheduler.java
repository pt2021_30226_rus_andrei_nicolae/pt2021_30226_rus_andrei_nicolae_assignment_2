package model;
import java.util.ArrayList;
import java.util.List;
// clasa specifica de a aranja elementele in cozi
public class Scheduler {
    private List<Server> servers; // lista de servere/cozi
    private int maxNoServers; // nr max de servere
    private Strategy strategy; // tipul de startegie de asezare in cozi
    private int maxTasksPerServer; // optional nr maxim de clienti pe server

    public Scheduler(int maxNoServers,int maxTasksPerServer) // constructor de initializare
    {
        this.maxNoServers = maxNoServers;
        this.maxTasksPerServer = maxTasksPerServer;
        strategy = new ConcreteStategyQueue();
        servers = new ArrayList<Server>();

        for(int i=0 ; i<this.maxNoServers ; i++) // pentru toate cozile
        {
            Server s = new Server(); // creez noua coada
            servers.add(s); // o adaug in servere
            Thread t = new Thread(s); // pornesc un nou thread pentru fiecare coada
            t.start();
        }
    }
    public void changeStrategy (SelectionPolicy policy) // selectez strategia de asezare in coada
    {
        if (policy == SelectionPolicy.SHORTEST_QUEUE) // strategie coada goala
            strategy = new ConcreteStategyQueue();
        if (policy == SelectionPolicy.SHORTEST_TIME) // strategie coada timp scurt
            strategy = new ConcreteStrategyTime();
    }
    public void dispatchTask(Task t)  {
         strategy.addTask(servers, t);
    } // adaugarea unui client conform strategie
    public List<Server> getServers()
    {
        return servers;
    } // getter servere
    public void stopServers() // oprire cozi
    {
        for(Server s : servers)
        {
            s.stop();
        }
    }
    public boolean serversFull() // verifica daca cozile sunt toate pline (au cel putin un client in ea)
    {
        for(Server s : servers)
        {
            if(s.getTasks().isEmpty())
                return false;
        }
        return true;
    }

}
