package view;

import javax.swing.*;
// clasa pentru afisarea rezultatelor cu o noua interfata
public class ViewOutput extends JFrame {
    private JLabel label;

    public ViewOutput()
    {
        label = new JLabel(" "); // locul unde se vor afisa rezultatele
        this.setSize(500,500);
        this.add(label);
        this.setTitle("RESULTS");
        this.setVisible(true);
    }
    public void setText(String s)
    {
        label.setText(s);
    } // setare text
}
