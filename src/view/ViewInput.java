package view;

import javax.swing.*;
import java.awt.event.ActionListener;
// clasa pentru introducerea datelor de catre utilizator
public class ViewInput extends JFrame {
    private JTextField nrClients,queues,simulationInterval; // intrari pt nr clienti, cozi, interval
    private JTextField minArrival,maxArrival;   // timp de ajungere min si max
    private JTextField minService,maxService;   // timp minim/maxim service
    private JLabel lnrClients,lQueues,lSimulationInterval;  // label pt aceleasi date de mai sus
    private JLabel lMinArrival,lMaxArrival;
    private JLabel lMinService,lMaxService;
    private JButton start; // buton pentru confirmare 
    private JPanel p1,p2,p3,p4,p5,p6;
    public ViewInput()
    {
        this.nrClients = new JTextField(5);
        this.queues = new JTextField(5);
        this.simulationInterval = new JTextField(5);
        this.minArrival = new JTextField(3);
        this.maxService = new JTextField(3);
        this.minService = new JTextField(3);
        this.maxArrival = new JTextField(3);

        this.lnrClients = new JLabel("Nr. of clients:");
        this.lQueues = new JLabel("Nr. of queues:");
        this.lSimulationInterval = new JLabel("Simulation Time:");
        this.lMinArrival = new JLabel("Arrival time- MIN:");
        this.lMaxArrival = new JLabel(" MAX:");
        this.lMinService = new JLabel("Service time- MIN:");
        this.lMaxService = new JLabel(" MAX");
        this.start = new JButton("START");

        this.p1 = new JPanel();
        this.p2 = new JPanel();
        this.p3 = new JPanel();
        this.p4 = new JPanel();
        this.p5 = new JPanel();
        this.p6 = new JPanel();
        p1.add(lnrClients);
        p1.add(nrClients);
        p2.add(lQueues);
        p2.add(queues);
        p3.add(lSimulationInterval);
        p3.add(simulationInterval);
        p4.add(lMinArrival);
        p4.add(minArrival);
        p4.add(lMaxArrival);
        p4.add(maxArrival);
        p5.add(lMinService);
        p5.add(minService);
        p5.add(lMaxService);
        p5.add(maxService);
        p6.add(start);
        this.setContentPane(p1);
        this.add(p2);
        this.add(p3);
        this.add(p4);
        this.add(p5);
        this.add(p6);
        this.setSize(500,500);
        this.setTitle("SIMULATOR");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public void addListener(ActionListener e)
    {
        this.start.addActionListener(e);
    }

    public JTextField getNrClients() {
        return nrClients;
    }

    public JTextField getSimulationInterval() {
        return simulationInterval;
    }

    public JTextField getQueues() {
        return queues;
    }
    public JTextField getMinArrival() {
        return minArrival;
    }

    public JTextField getMaxArrival() {
        return maxArrival;
    }

    public JTextField getMinService() {
        return minService;
    }

    public JTextField getMaxService() {
        return maxService;
    }
}
