package controller;
import model.*;
import view.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
// clasa care controleaza tot programul
public class Controller {
    private SimulationManager simulationManager; // camp care reprezinta procesul de simulare
    public ViewInput viewInput; // interfata grafica pentru date de intrare

    public Controller() // initizalizarea campurilor
    {
        viewInput = new ViewInput();
        viewInput.addListener(new Happens()); // initializare buton cu ascultator Listener
    }
    class Happens implements ActionListener // clasa interna pentru aparitia unui eveniment
    {
        public void actionPerformed(ActionEvent event)
        {
            try { //
                simulationManager = new SimulationManager(viewInput); //  se creaza obiectul de simulare
            } catch (IOException e) {
                e.printStackTrace();
            }
            Thread t = new Thread(simulationManager); //  se porneste thread-ul principal cu timpii respectivi
            t.start();
        }
    }
}
